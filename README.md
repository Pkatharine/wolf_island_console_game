# :wolf: WolfIsland
[![cpp-standard-style](https://img.shields.io/badge/code%20style-Google-brightgreen.svg?style=flat)](https://google.github.io/styleguide/cppguide.html)

<h1 align="center">
  <br>
  <img src="https://github.com/path-to/logo.png" alt="WolfIsland" width="160">
</h1>

<h4 align="center">A desktop app for WolfIsland environment modeling based on <a href="https://www.sfml-dev.org" target="_blank">SFML</a>.</h4>


Simulate artificial the life of animals in closed environment by changing the parameters and 
watch for changes in their population.

## Demo

![wolf-island-demo](https://github.com/path-to/X.gif)

## Motivation
The project exists in purpose to show the ecosystem of the island and what is happening there, 
to give anyone the opportunity to try, by changing the parameters, managing the life and seeing 
positive and harmful changes in the population of animals that live there.

## Features

TBD

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development
and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to prepare before run this project.

```
TBD: examples
```

### Installing

TBD

## Deployment

TBD: docker deployment.

## Built With

* [SFML](https://www.sfml-dev.org/) - Simple and Fast Multimedia Library
* [TGUI](https://tgui.eu/) - An easy to use cross-platform C++ GUI library for SFML
* [CLion](https://www.jetbrains.com/clion/) - One of the best IDE for developing with C++

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our
code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see 
the [tags on this repository](https://github.com//targetflow/WolfIsland/tags). 

## Authors

* **Katya Pereverzeva** - *initiator, dev, sup* - [PKatharine](https://github.com/PKatharine)
* **Alex Orlovskyi** - *dev, pm, sup* - [NeshkoO](https://github.com/NeshkoO)

## License

This project is licensed under the terms of the GNU General Public License, GPLv3 - see the 
[LICENSE](LICENSE) file for details.

## Acknowledgments

* We greatly appreciate 👍👍 all incoming feedback about this project. 
Together we continiously are making it better and better 😌
